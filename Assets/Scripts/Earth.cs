﻿using UnityEngine;
using System.Collections.Generic;

public class Earth : MonoBehaviour {
	#region Variables
    public static Earth     THIS;
    public GameObject       pulse;

    // damage
    [SerializeField] 
    private SpriteRenderer  damageIndicator;
    private int             damage;
    [SerializeField]
    private List<Sprite>    damageSprites;

    // timer
    [SerializeField] 
    private TextMesh        display;
    public float            timer;
	#endregion

	#region Monobehaviour Methods
	void Awake () {
        THIS = this;
        pulse = GameObject.Find("Planet Pulse").gameObject;
	}
	
	void Update () {
        UpdateTimer();
	}
	#endregion

	#region Methods
    public void Hit() {
        damage++;
        ScreenShake.THIS.Shake();
        if (damage < 4)
        {
            damageIndicator.sprite = damageSprites[damage];
        }
        else
        {
            GameplayManager.instance.SetScoreScreen();
        }
    }

    public void Hide() {
        THIS.gameObject.SetActive(false);
    }

    public void Reset() {
        damage = 0;
        timer = 0;
        damageIndicator.sprite = damageSprites[0];
    }

    private void UpdateTimer() {
        timer += Time.deltaTime;
        display.text = Mathf.RoundToInt(timer).ToString();
    }
	#endregion
}
