﻿using UnityEngine;
using System.Collections;

public class RhytmManager : MonoBehaviour {

    public static RhytmManager instance;
    public float globalRhytm;
    public float rhytm;
    public int bar = 5;
    public int count = 1;
    public bool beat;
    public bool loop;

    private bool tickLock;
    public float            BGScale = 1;

    public Transform[]      pulsatingThings;       

	void Start () {
        instance = this;
     //   pulsatingBG = GameObject.Find("Lowpoly Pulse").transform;
	}
	
	void Update () {
        beat = loop = false;
        rhytm -= Time.deltaTime;
        if (rhytm <= 0)
        {
            rhytm = globalRhytm;
            beat = true;
            count++;
            if (count > bar)
            {
                count = 1;
                loop = true;
            }
        }
        
        Pulse();

        if (GameStateManager.instance.currentGameState == GameState.Game)
        {
            float timer = Earth.THIS.timer;
            // bar change
            if (timer < 10)         bar = 4;
            else if (timer < 40 )   bar = 3;
            else if (timer < 86)    bar = 2;
            else                    bar = 1;

            // double spawn chance
            if (timer < 5)          Spawner.instance.doubleSpawnChance = 0.0f;
            else if (timer < 10)    Spawner.instance.doubleSpawnChance = 0.15f;
            else if (timer < 25)    Spawner.instance.doubleSpawnChance = 0.4f;
            else if (timer < 40)    Spawner.instance.doubleSpawnChance = 0.1f;
            else if (timer < 55)    Spawner.instance.doubleSpawnChance = 0.3f;
            else if (timer < 70)    Spawner.instance.doubleSpawnChance = 0.5f;
            else if (timer < 85)    Spawner.instance.doubleSpawnChance = 0f;
            else if (timer < 100)   Spawner.instance.doubleSpawnChance = 0.05f;
            else if (timer < 115)   Spawner.instance.doubleSpawnChance = 0.10f;
            else if (timer < 130)   Spawner.instance.doubleSpawnChance = 0.15f;
            else if (timer < 145)   Spawner.instance.doubleSpawnChance = 0.2f;
            else if (timer < 160)   Spawner.instance.doubleSpawnChance = 0.3f;
            else                    Spawner.instance.doubleSpawnChance = 0.4f;
        }
	}

    void Pulse() {
        float scale = 1 + (BGScale - 1) * rhytm / globalRhytm;
        foreach (Transform t in pulsatingThings)
            t.localScale = new Vector3(scale, scale);
    }
}
