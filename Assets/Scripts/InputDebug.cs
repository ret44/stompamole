﻿using UnityEngine;
using System.Collections;

public class InputDebug : MonoBehaviour {

    public GUIText debugText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (DancePad.GetButton(PadButton.UpLeft)) 
            debugText.text = "UP LEFT";
        if (DancePad.GetButton(PadButton.Up)) 
            debugText.text = "UP";
        if (DancePad.GetButton(PadButton.UpRight)) 
            debugText.text = "UP RIGHT";
        if (DancePad.GetButton(PadButton.Left)) 
            debugText.text = "LEFT";
        if (DancePad.GetButton(PadButton.Right)) 
            debugText.text = "RIGHT";
        if (DancePad.GetButton(PadButton.DownLeft)) 
            debugText.text = "DOWN LEFT";
        if (DancePad.GetButton(PadButton.Down)) 
            debugText.text = "DOWN";
        if (DancePad.GetButton(PadButton.DownRight)) 
            debugText.text = "DOWN RIGHT";
        if (DancePad.GetButton(PadButton.Start)) 
            debugText.text = "START";
        if (DancePad.GetButton(PadButton.Select)) 
            debugText.text = "SELECT";
	}
}
