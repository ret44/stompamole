﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Gameplay : MonoBehaviour
{

    public static Gameplay instance;

    public float hideInterval;
    private float timeToHide;

    public int maxMolesPerTime;

    public int hit;
    public int miss;

    public GameObject stompPrefab;

    public List<Mole> molesShown;

    public int legSpeed;

    public Mole upLeftMole;
    public Mole upMole;
    public Mole upRightMole;
    public Mole leftMole;
    public Mole rightMole;
    public Mole downLeftMole;
    public Mole downMole;
    public Mole downRightMole;

    public Mole GetMole(int val)
    {
        switch (val)
        {
            case 0: return this.upLeftMole;
            case 1: return this.upMole;
            case 2: return this.upRightMole;
            case 3: return this.leftMole;
            case 4: return this.rightMole;
            case 5: return this.downLeftMole;
            case 6: return this.downMole;
            case 7: return this.downRightMole;
            default: return null;
        }
    }


    void ShowMoles()
    {
        //Hide previous moles
        foreach (Mole mole in molesShown)
        {
            mole.Hide();
            miss++;
        }
        molesShown.Clear();

        //Show random moles
        for (int i = 0; i < Random.Range(1, maxMolesPerTime + 1); i++)
        {
            Mole nextMole = GetMole(Random.Range(0, 8));
            nextMole.Show();
            molesShown.Add(nextMole);
        }
    }

    // Use this for initialization
    void Start()
    {
        Application.targetFrameRate = 24;
        Gameplay.instance = this;
        if (maxMolesPerTime < 1) maxMolesPerTime = 1;
    }

    // Update is called once per frame
    void Update()
    {
            

            #region OldCode
            ////UpLeft
            //if (DancePad.GetButton(PadButton.UpLeft))
            //{
            //    if (!upLeftMole.isUp) upLeftMole.Show();
            //}
            //else
            //{
            //    if (upLeftMole.isUp) upLeftMole.Hide();
            //}

            ////Up
            //if (DancePad.GetButton(PadButton.Up))
            //{
            //    if (!upMole.isUp) upMole.Show();
            //}
            //else
            //{
            //    if (upMole.isUp) upMole.Hide();
            //}

            ////UpRight
            //if (DancePad.GetButton(PadButton.UpRight))
            //{
            //    if (!upRightMole.isUp) upRightMole.Show();
            //}
            //else
            //{
            //    if (upRightMole.isUp) upRightMole.Hide();
            //}

            ////Left
            //if (DancePad.GetButton(PadButton.Left))
            //{
            //    if (!leftMole.isUp) leftMole.Show();
            //}
            //else
            //{
            //    if (leftMole.isUp) leftMole.Hide();
            //}

            ////Right
            //if (DancePad.GetButton(PadButton.Right))
            //{
            //    if (!rightMole.isUp) rightMole.Show();
            //}
            //else
            //{
            //    if (rightMole.isUp) rightMole.Hide();
            //}

            ////DownLeft
            //if (DancePad.GetButton(PadButton.DownLeft))
            //{
            //    if (!downLeftMole.isUp) downLeftMole.Show();
            //}
            //else
            //{
            //    if (downLeftMole.isUp) downLeftMole.Hide();
            //}

            ////Down
            //if (DancePad.GetButton(PadButton.Down))
            //{
            //    if (!downMole.isUp) downMole.Show();
            //}
            //else
            //{
            //    if (downMole.isUp) downMole.Hide();
            //}

            ////DownRight
            //if (DancePad.GetButton(PadButton.DownRight))
            //{
            //    if (!downRightMole.isUp) downRightMole.Show();
            //}
            //else
            //{
            //    if (downRightMole.isUp) downRightMole.Hide();
            //}

            //timeToHide -= Time.deltaTime;
            //if (timeToHide <= 0)
            //{
            //    timeToHide = hideInterval;
            //    ShowMoles();
            //}
            //if (DancePad.GetButtonDown(PadButton.UpLeft))
            //{
            //    Instantiate(stompPrefab, new Vector3(upLeftMole.transform.position.x, 20f, upLeftMole.transform.position.z), Quaternion.identity);
            //    if (upLeftMole.isUp)
            //    {
            //        hit++;
            //        upLeftMole.Hide();
            //    }
            //}
            //if (DancePad.GetButtonDown(PadButton.Up))
            //{
            //    Instantiate(stompPrefab, new Vector3(upMole.transform.position.x, 20f, upMole.transform.position.z), Quaternion.identity);
            //    if (upMole.isUp)
            //    {
            //        hit++;
            //        upMole.Hide();
            //    }
            //}
            //if (DancePad.GetButtonDown(PadButton.UpRight))
            //{
            //    Instantiate(stompPrefab, new Vector3(upRightMole.transform.position.x, 20f, upRightMole.transform.position.z), Quaternion.identity);
            //    if (upRightMole.isUp)
            //    {
            //        hit++;
            //        upRightMole.Hide();
            //    }
            //}
            //if (DancePad.GetButtonDown(PadButton.Left))
            //{
            //    Instantiate(stompPrefab, new Vector3(leftMole.transform.position.x, 20f, leftMole.transform.position.z), Quaternion.identity);
            //    if (leftMole.isUp)
            //    {
            //        hit++;
            //        leftMole.Hide();
            //    }
            //}
            //if (DancePad.GetButtonDown(PadButton.Right))
            //{
            //    Instantiate(stompPrefab, new Vector3(rightMole.transform.position.x, 20f, rightMole.transform.position.z), Quaternion.identity);
            //    if (rightMole.isUp)
            //    {
            //        hit++;
            //        rightMole.Hide();
            //    }
            //}
            //if (DancePad.GetButtonDown(PadButton.DownLeft))
            //{
            //    Instantiate(stompPrefab, new Vector3(downLeftMole.transform.position.x, 20f, downLeftMole.transform.position.z), Quaternion.identity);
            //    if (downLeftMole.isUp)
            //    {
            //        hit++;
            //        downLeftMole.Hide();
            //    }
            //}
            //if (DancePad.GetButtonDown(PadButton.Down))
            //{
            //    Instantiate(stompPrefab, new Vector3(downMole.transform.position.x, 20f, downMole.transform.position.z), Quaternion.identity);
            //    if (downMole.isUp)
            //    {
            //        hit++;
            //        downMole.Hide();
            //    }
            //}
            //if (DancePad.GetButtonDown(PadButton.DownRight))
            //{
            //    Instantiate(stompPrefab, new Vector3(downRightMole.transform.position.x, 20f, downRightMole.transform.position.z), Quaternion.identity);
            //    if (downRightMole.isUp)
            //    {
            //        hit++;
            //        downRightMole.Hide();
            //    }
            //}
            #endregion
        

    }
}
