﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    public static Spawner instance;

    public Transform UpLeftPoint;
    public Transform UpPoint;
    public Transform UpRightPoint;
    public Transform LeftPoint;
    public Transform RightPoint;
    public Transform DownLeftPoint;
    public Transform DownPoint;
    public Transform DownRightPoint;

    public GameObject asteroidPrefab;

    public float spawningDistance;

    public float maxSpawnTime;
    public float minSpawnTime;

    public float        doubleSpawnChance;
    private Transform   lastSpawnPoint;

    public Transform GetPoint(int val)
    {
        switch (val)
        {
            case 0: return this.UpPoint;
            case 1: return this.LeftPoint;
            case 2: return this.RightPoint;
            case 3: return this.DownPoint;
            case 4: return this.UpLeftPoint;
            case 5: return this.DownLeftPoint;
            case 6: return this.UpRightPoint;
            case 7: return this.DownRightPoint;
            default: return null;
        }
    }

    
	// Use this for initialization
	void Start () {
        instance = this;
        doubleSpawnChance = 0.1f;

        UpLeftPoint.position = (Vector3.up + Vector3.left).normalized * spawningDistance;
        UpPoint.position = Vector3.up * spawningDistance;
        UpRightPoint.position = (Vector3.up + Vector3.right).normalized * spawningDistance;
        LeftPoint.position = Vector3.left * spawningDistance;
        RightPoint.position = Vector3.right * spawningDistance;
        DownLeftPoint.position = (Vector3.down + Vector3.left).normalized * spawningDistance;
        DownPoint.position = Vector3.down * spawningDistance;
        DownRightPoint.position = (Vector3.down + Vector3.right).normalized * spawningDistance;
	}
	
	// Update is called once per frame
	void Update () {
        if (RhytmManager.instance.loop && GameStateManager.instance.currentGameState == GameState.Game)
        {
            Spawn();
            if (Random.Range(0f, 1f) < doubleSpawnChance)
                Spawn();

            lastSpawnPoint = null;
        }
	}

    void Spawn() {
        Transform start = GetPoint(Random.Range(0, (Earth.THIS.timer < 25 ? 4 : 8)));
        if (lastSpawnPoint != null) {
            while (start == lastSpawnPoint) {
                start = GetPoint(Random.Range(0, (Earth.THIS.timer < 25 ? 4 : 8)));
            }
        }

        lastSpawnPoint = start;
        GameObject asteroid = Instantiate(asteroidPrefab, start.position, Quaternion.identity) as GameObject;
        asteroid.GetComponent<Asteroid>().motherbase = start.position;
    }
}
