﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Stomp : MonoBehaviour {
 
    public bool goingUp;
	// Use this for initialization
	void Start () {
        transform.DOLocalMoveY(0.0f, 0.15f).OnComplete(GoUp);
	}
	
    void GoUp()
    {
        transform.DOLocalMoveY(20f, 0.15f).OnComplete(Die);
    }

    void Die()
    {
        Destroy(this.gameObject);
    }

	// Update is called once per frame
	void Update () {
	   
	}
}
