﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Asteroid : MonoBehaviour {

    public Vector3 motherbase;
    public Vector3 target;
    public float speed;
    public float rhytm;

    public Color            color;
    private SpriteRenderer  sr;
    private SpriteRenderer  pulse;

    public float lastRhytm;
    public float autoDestructTime;

	void Start () {
        pulse = transform.GetChild(0).GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;

        sr = GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
        color = new Color(Random.Range(0.5f, 1f), Random.Range(0.5f, 1f), Random.Range(0.5f, 1f));
        sr.color = color;
        pulse.color = new Color(color.r, color.g, color.b, 0.3f);
	}

    void OnTriggerEnter(Collider col)
    {
        transform.DOKill();
        if (col.gameObject.tag == "Paddle")
        {
            Transform t = col.transform.GetChild(0);
            t.particleSystem.renderer.material.color = color;
            t.particleSystem.Play();

            //GameObject.Find("Gradient").GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b);
            ColorManager.SetColor(color);
            Destroy(gameObject);
        }

        if (col.gameObject.tag == "Planet") {
            Earth.THIS.Hit();
            Destroy(gameObject);
        }
    }

    public void GoThrough()
    {
        target = -motherbase;
    }

	void Update () {
        if (target != motherbase)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, ((1-(RhytmManager.instance.rhytm / RhytmManager.instance.globalRhytm) )* Time.deltaTime * speed));
            float scale = 1 + (1.5f - 1) * RhytmManager.instance.rhytm / RhytmManager.instance.globalRhytm;
            pulse.transform.localScale = new Vector3(scale, scale);
        }
       // 
        
        autoDestructTime -= Time.deltaTime;
        if (autoDestructTime <= 0) Destroy(gameObject);
	}
}
