﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

    public PadButton padButton;
    public Transform orientationPoint;
    public Transform paddleSprite;
    public ParticleSystem particles;


    public float activeTime;

    public float timer;

    public bool active;

	// Use this for initialization
	void Start () {
        if (orientationPoint.position != transform.position)
        {
            Vector3 lookPos = orientationPoint.position - transform.position;
            float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        activeTime = RhytmManager.instance.globalRhytm; 
        timer = activeTime;
        paddleSprite = transform.GetChild(0);
        particles = paddleSprite.GetChild(0).particleSystem;
	}
	
	// Update is called once per frame
	void Update () {
        active = DancePad.GetButton(padButton);
        float speed = 5;

        if (!active) timer = activeTime;

        if (active && timer>0)
        {
            if (paddleSprite.localScale.x < 1)
                paddleSprite.localScale = new Vector3(paddleSprite.localScale.x + Time.deltaTime * speed, paddleSprite.localScale.y, paddleSprite.localScale.z);
            if (paddleSprite.localScale.x > 1)
                paddleSprite.localScale = new Vector3(1, paddleSprite.localScale.y, paddleSprite.localScale.z);
            paddleSprite.gameObject.collider.enabled = true;
            timer -= Time.deltaTime;
        }
        else
        {
            
                if (paddleSprite.localScale.x > 0)
                    paddleSprite.localScale = new Vector3(paddleSprite.localScale.x - Time.deltaTime * speed, paddleSprite.localScale.y, paddleSprite.localScale.z);
                if(paddleSprite.localScale.x < 0)
                    paddleSprite.localScale = new Vector3(0, paddleSprite.localScale.y, paddleSprite.localScale.z);
                paddleSprite.gameObject.collider.enabled = false;           
        }
	}
}
