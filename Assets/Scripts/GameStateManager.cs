﻿using UnityEngine;
using System.Collections;

public enum GameState { Title, Instructions, Game, Score, Calibration }

public class GameStateManager : MonoBehaviour {

    public static GameStateManager instance;
    public GameState currentGameState;
	// Use this for initialization
	void Start () {
        GameStateManager.instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
