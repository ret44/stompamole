﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour {
	#region Variables
    public static ScreenShake   THIS;

    private float       shakeValue      = 0.6f;
    private float       shakeSpeed      = 0.05f;
    private float       shakeThreshold;
	#endregion

	#region Monobehaviour Methods
	void Awake () {
        THIS = this;
	}
	
	void Update () {
	    if (shakeThreshold > 0) {
            shakeThreshold -= shakeSpeed;
            transform.position = new Vector3(Random.Range(-shakeThreshold, shakeThreshold), Random.Range(-shakeThreshold, shakeThreshold), transform.position.z);
	    }
	}
	#endregion

	#region Methods
    public void Shake() {
        shakeThreshold = shakeValue;
    }
	#endregion
}
