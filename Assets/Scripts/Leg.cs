﻿using UnityEngine;
using DG.Tweening;
using System.Collections;


public enum LegState {  Waiting, Down, Up}
public class Leg : MonoBehaviour {

    public float delayTime;
    public LegState currentState;

    public Vector3 basePosition;
    public Vector3 impactPosition;

	// Use this for initialization
	void Start () {
        delayTime = Random.Range(3f, 5f);
        currentState = LegState.Waiting;
	}
	
	// Update is called once per frame
	void Update () {
        if(currentState == LegState.Waiting)
        {
            delayTime -= Time.deltaTime;
            if (delayTime <= 0)
            {
                currentState = LegState.Down;
                transform.DOLocalMove(impactPosition, Gameplay.instance.legSpeed);
            }
        }
        if(currentState == LegState.Down)
        {

        }
	}
}
