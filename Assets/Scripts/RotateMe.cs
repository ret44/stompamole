﻿using UnityEngine;
using System.Collections;

public class RotateMe : MonoBehaviour {

    public float rotationSpeed;
    public bool randomize = false;

	// Use this for initialization
	void Start () {
        if (randomize)
            rotationSpeed = Random.Range(rotationSpeed / 2, rotationSpeed);
	}
	
	// Update is called once per frame
	void Update () {
        //this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z - (rotationSpeed * Time.deltaTime), this.transform.rotation.w);
		this.transform.Rotate (0,0,rotationSpeed * Time.deltaTime,Space.Self);
	}
}
