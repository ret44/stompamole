﻿using UnityEngine;
using System.Collections;

public enum PadButton { UpLeft, Up, UpRight,Left,Right,DownLeft,Down,DownRight,Select,Start };

public class DancePad {

    public static int UpLeftCode = 6;
    public static int UpCode = 2;
    public static int UpRightCode = 7;
    public static int LeftCode = 0;
    public static int RightCode = 3;
    public static int DownLeftCode = 5;
    public static int DownCode = 1;
    public static int DownRightCode = 4;
    public static int SelectCode = 8;
    public static int StartCode = 9;

    public static bool GetButton(PadButton button)
    {
        switch (button)
        {
            case PadButton.UpLeft: if (Input.GetKey("joystick button "+UpLeftCode.ToString()) || Input.GetKey(KeyCode.Keypad7) || Input.GetKey(KeyCode.E)) return true; break;
            case PadButton.Up: if (Input.GetKey("joystick button " + UpCode.ToString()) || Input.GetKey(KeyCode.Keypad8) || Input.GetKey(KeyCode.R)) return true; break;
            case PadButton.UpRight: if (Input.GetKey("joystick button " + UpRightCode.ToString()) || Input.GetKey(KeyCode.Keypad9) || Input.GetKey(KeyCode.T)) return true; break;
            case PadButton.Left: if (Input.GetKey("joystick button " + LeftCode.ToString()) || Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.D)) return true; break;
            case PadButton.Right: if (Input.GetKey("joystick button " + RightCode.ToString()) || Input.GetKey(KeyCode.Keypad6) || Input.GetKey(KeyCode.G)) return true; break;
            case PadButton.DownLeft: if (Input.GetKey("joystick button " + DownLeftCode.ToString()) || Input.GetKey(KeyCode.Keypad1) || Input.GetKey(KeyCode.C)) return true; break;
            case PadButton.Down: if (Input.GetKey("joystick button " + DownCode.ToString()) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.V)) return true; break;
            case PadButton.DownRight: if (Input.GetKey("joystick button " + DownRightCode.ToString()) || Input.GetKey(KeyCode.Keypad3) || Input.GetKey(KeyCode.B)) return true; break;
            case PadButton.Select: if (Input.GetKey("joystick button " + SelectCode.ToString()) || Input.GetKey(KeyCode.Keypad0) || Input.GetKey(KeyCode.Alpha3)) return true; break;
            case PadButton.Start: if (Input.GetKey("joystick button " + StartCode.ToString()) || Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey(KeyCode.Alpha5)) return true; break;
            default: return false;
        }
        return false;
    }

    public static bool GetButtonDown(PadButton button)
    {
        switch (button)
        {
            case PadButton.UpLeft: if (Input.GetKeyDown("joystick button " + UpLeftCode.ToString())) return true; break;
            case PadButton.Up: if (Input.GetKeyDown("joystick button " + UpCode.ToString())) return true; break;
            case PadButton.UpRight: if (Input.GetKeyDown("joystick button " + UpRightCode.ToString())) return true; break;
            case PadButton.Left: if (Input.GetKeyDown("joystick button " + LeftCode.ToString())) return true; break;
            case PadButton.Right: if (Input.GetKeyDown("joystick button " + RightCode.ToString())) return true; break;
            case PadButton.DownLeft: if (Input.GetKeyDown("joystick button " + DownLeftCode.ToString())) return true; break;
            case PadButton.Down: if (Input.GetKeyDown("joystick button " + DownCode.ToString())) return true; break;
            case PadButton.DownRight: if (Input.GetKeyDown("joystick button " + DownRightCode.ToString())) return true; break;
            case PadButton.Select: if (Input.GetKeyDown("joystick button " + SelectCode.ToString())) return true; break;
            case PadButton.Start: if (Input.GetKeyDown("joystick button " + StartCode.ToString())) return true; break;
            default: return false;
        }
        return false;
    }

    public static bool GetButtonUp(PadButton button)
    {
        switch (button)
        {
            case PadButton.UpLeft: if (Input.GetKeyUp("joystick button " + UpLeftCode.ToString())) return true; break;
            case PadButton.Up: if (Input.GetKeyUp("joystick button " + UpCode.ToString())) return true; break;
            case PadButton.UpRight: if (Input.GetKeyUp("joystick button " + UpRightCode.ToString())) return true; break;
            case PadButton.Left: if (Input.GetKeyUp("joystick button " + LeftCode.ToString())) return true; break;
            case PadButton.Right: if (Input.GetKeyUp("joystick button " + RightCode.ToString())) return true; break;
            case PadButton.DownLeft: if (Input.GetKeyUp("joystick button " + DownLeftCode.ToString())) return true; break;
            case PadButton.Down: if (Input.GetKeyUp("joystick button " + DownCode.ToString())) return true; break;
            case PadButton.DownRight: if (Input.GetKeyUp("joystick button " + DownRightCode.ToString())) return true; break;
            case PadButton.Select: if (Input.GetKeyUp("joystick button " + SelectCode.ToString())) return true; break;
            case PadButton.Start: if (Input.GetKeyUp("joystick button " + StartCode.ToString())) return true; break;
            default: return false;
        }
        return false;
    }

}
