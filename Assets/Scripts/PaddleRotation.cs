﻿using UnityEngine;
using System.Collections;

public class PaddleRotation : MonoBehaviour {

  	// Use this for initialization
	void Start () {
      
	}

    public void LookTowards(Transform direction)
    {
        if (direction.position != transform.position)
        {
            Vector3 lookPos = direction.position - transform.position;
            float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    void Update()
    {

    }
}
