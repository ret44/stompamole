﻿using UnityEngine;
using System.Collections;

public class LateFadeOut : MonoBehaviour {

    public bool startFade;
    public float timeBeforeFade;

    public void Reset()
    {        
        startFade = true;
        timeBeforeFade = 2;
        renderer.material.color = new Color(1, 1, 1);
    }


	// Use this for initialization
	void Start () {
        Reset();
	}
	
	// Update is called once per frame
	void Update () {
	    if(startFade)
        {
            timeBeforeFade -= Time.deltaTime;
            if(timeBeforeFade<=0)
            {
                Color newCol = renderer.material.color;
                newCol.a -= Time.deltaTime * 0.5f;
                renderer.material.color = newCol;
            }
        }
	}
}
