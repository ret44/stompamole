﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {

    public bool isFadingOut;
    public float fadingStep;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (isFadingOut)
        {
            this.renderer.material.color = new Color(255, 255, 255, this.renderer.material.color.a - (Time.deltaTime * fadingStep));
        }
	}
}
