﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Mole : MonoBehaviour {

    public bool isUp;

    public void Show()
    {
        if (!isUp)
        {
            transform.DOLocalMoveY(0.5f, 0.25f);
            isUp = true;
        }
    }

    public void Hide()
    {
        if (isUp)
        {
            transform.DOLocalMoveY(-0.5f, 0.25f);
            isUp = false;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
