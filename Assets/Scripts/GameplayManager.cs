﻿using UnityEngine;
using System.Collections;

public class GameplayManager : MonoBehaviour
{

    public static GameplayManager instance;
    public GameObject titleScreen;
    public GameObject gameScreen;
    public GameObject scoreScreen;

    public GUIText playedCounter;

    public LateFadeOut arrowsFade;

    public ParticleSystem earthExplosionEffect;

    public GameObject[] scoreScreenBeats;
    private int scoreScreenBeatCount;

    public float attractTimer;

    void Start()
    {
        instance = this;
    }

    public void SetTitleScreen()
    {
        titleScreen.SetActive(true);
        gameScreen.SetActive(false);
        scoreScreen.SetActive(false);
        RhytmManager.instance.globalRhytm = 0.94f;       
        Music.instance.ingame.Stop();
        Music.instance.menu.Play();
        GameStateManager.instance.currentGameState = GameState.Title;
        GameObject[] asteroids = GameObject.FindGameObjectsWithTag("Asteroid");
        Earth.THIS.gameObject.SetActive(false);
        Earth.THIS.pulse.SetActive(false);

        for (int i = 0; i < asteroids.Length; i++)
        {
            Destroy(asteroids[i]);
        }
    }

    public void SetGameScreen()
    {
        PlayerPrefs.SetInt("played", PlayerPrefs.GetInt("played") + 1);     
        GameObject[] asteroids = GameObject.FindGameObjectsWithTag("Asteroid");
        for (int i = 0; i < asteroids.Length; i++)
        {
            Destroy(asteroids[i]);
        }
        titleScreen.SetActive(false);
        gameScreen.SetActive(true);
        scoreScreen.SetActive(false);
        RhytmManager.instance.globalRhytm = 0.47f;
        RhytmManager.instance.rhytm = 0.47f;
        RhytmManager.instance.bar = 5;
        arrowsFade.Reset();
        arrowsFade.startFade = true;
        Music.instance.menu.Stop();
        Music.instance.ingame.Play();
        GameStateManager.instance.currentGameState = GameState.Game;
        Earth.THIS.Reset();
        Earth.THIS.gameObject.SetActive(true);
        Earth.THIS.pulse.SetActive(true);
    }

    public void SetScoreScreen()
    {
        scoreScreen.SetActive(true);
        GameStateManager.instance.currentGameState = GameState.Score;
        int score = Mathf.RoundToInt(Earth.THIS.timer);
        int highscore = PlayerPrefs.GetInt("highScore",0);
        if (score > highscore)  PlayerPrefs.SetInt("highScore", score);        
        scoreScreenBeats[1].GetComponent<TextMesh>().text = score.ToString();
        scoreScreenBeats[2].GetComponent<TextMesh>().text = (score > highscore ? score.ToString() + "!" : highscore.ToString());
        if (score > highscore) PlayerPrefs.SetInt("highScore", score);
        earthExplosionEffect.Play();
        Earth.THIS.Invoke("Hide", 0.15f);
        Earth.THIS.pulse.SetActive(false);
        GameObject[] asteroids = GameObject.FindGameObjectsWithTag("Asteroid");
        for (int i = 0; i < asteroids.Length; i++)
        {
            asteroids[i].GetComponent<Asteroid>().GoThrough();
        }
        scoreScreenBeatCount = 0;
        for (int i = 0; i < scoreScreenBeats.Length; i++)
        {
            scoreScreenBeats[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (DancePad.GetButton(PadButton.Select) && DancePad.GetButton(PadButton.Down)) playedCounter.text = "Played " + PlayerPrefs.GetInt("played") + " times";
        else playedCounter.text = "";
            attractTimer += Time.deltaTime;
        if (Input.anyKey) attractTimer = 0;
        if (attractTimer > 90 && GameStateManager.instance.currentGameState != GameState.Title) SetTitleScreen();

		if(Input.GetKey (KeyCode.Escape)) Application.Quit ();

        if (GameStateManager.instance.currentGameState == GameState.Title)
        {
            if (DancePad.GetButton(PadButton.Start))
            {
                SetGameScreen();
            }
        }

        if (GameStateManager.instance.currentGameState == GameState.Game)
        {

        }

        if(GameStateManager.instance.currentGameState == GameState.Score)
        {
            if (DancePad.GetButton(PadButton.Start))
            {
                SetGameScreen();
            }
            if(RhytmManager.instance.beat)
            {
                if(scoreScreenBeatCount<scoreScreenBeats.Length)
                {
                    scoreScreenBeats[scoreScreenBeatCount++].SetActive(true);
                }
            }
            if (Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.P)) {
                PlayerPrefs.SetInt("highScore", 0);
            }
		
        }

        //int pressCount = 0;

        //if (DancePad.GetButton(PadButton.UpLeft))
        //{
        //    if (pressCount == 0)
        //    {
        //        paddle1.LookTowards(Spawner.instance.UpLeftPoint);
        //        pressCount++;
        //    }
        //    else if (pressCount == 1)
        //    {
        //        paddle2.LookTowards(Spawner.instance.UpLeftPoint);
        //    }
        //}
        //if (DancePad.GetButton(PadButton.Up))
        //{
        //    if (pressCount == 0)
        //    {
        //        paddle1.LookTowards(Spawner.instance.UpPoint);
        //        pressCount++;
        //    }
        //    else if (pressCount == 1)
        //    {
        //        paddle2.LookTowards(Spawner.instance.UpPoint);
        //    }
        //}
        //if (DancePad.GetButton(PadButton.UpRight))
        //{

        //    if (pressCount == 0)
        //    {
        //        paddle1.LookTowards(Spawner.instance.UpRightPoint);
        //        pressCount++;
        //    }
        //    else if (pressCount == 1)
        //    {
        //        paddle2.LookTowards(Spawner.instance.UpRightPoint);
        //    }
        //}
        //if (DancePad.GetButton(PadButton.Left))
        //{
        //    if (pressCount == 0)
        //    {
        //        paddle1.LookTowards(Spawner.instance.LeftPoint);
        //        pressCount++;
        //    }
        //    else if (pressCount == 1)
        //    {
        //        paddle2.LookTowards(Spawner.instance.LeftPoint);
        //    }
        //}
        //if (DancePad.GetButton(PadButton.Right))
        //{
        //    if (pressCount == 0)
        //    {
        //        paddle1.LookTowards(Spawner.instance.RightPoint);
        //        pressCount++;
        //    }
        //    else if (pressCount == 1)
        //    {
        //        paddle2.LookTowards(Spawner.instance.RightPoint);
        //    }
        //}
        //if (DancePad.GetButton(PadButton.DownLeft))
        //{
        //    if (pressCount == 0)
        //    {
        //        paddle1.LookTowards(Spawner.instance.DownLeftPoint);
        //        pressCount++;
        //    }
        //    else if (pressCount == 1)
        //    {
        //        paddle2.LookTowards(Spawner.instance.DownLeftPoint);
        //    }
        //}
        //if (DancePad.GetButton(PadButton.Down))
        //{
        //    if (pressCount == 0)
        //    {
        //        paddle1.LookTowards(Spawner.instance.DownPoint);
        //        pressCount++;
        //    }
        //    else if (pressCount == 1)
        //    {
        //        paddle2.LookTowards(Spawner.instance.DownPoint);
        //    }
        //}
        //if (DancePad.GetButton(PadButton.DownRight))
        //{
        //    if (pressCount == 0)
        //    {
        //        paddle1.LookTowards(Spawner.instance.DownRightPoint);
        //        pressCount++;
        //    }
        //    else if (pressCount == 1)
        //    {
        //        paddle2.LookTowards(Spawner.instance.DownRightPoint);
        //    }
        //}

    }
}
