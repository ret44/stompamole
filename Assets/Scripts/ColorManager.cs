﻿using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using System.Collections;

public class ColorManager : MonoBehaviour {
	#region Variables
    public static ColorManager  THIS;
    public List<SpriteRenderer> colors;

    private float               speed = 0.08f;
    private static int          currentColor;
	#endregion

	#region Monobehaviour Methods
    void Start() {
        THIS = this;
    }

	void Update () {
        foreach (SpriteRenderer sr in colors)
            sr.transform.localScale = new Vector3(sr.transform.localScale.x + speed, sr.transform.localScale.y + speed, 1);
	}
	#endregion

	#region Methods
    public static void SetColor(Color color) {
        ResetColor(THIS.colors[currentColor], color);

        for (int i=0; i<THIS.colors.Count; i++) {
            THIS.colors[i].transform.localPosition = new Vector3(0, 0, i == currentColor ? 0 : THIS.colors[i].transform.localPosition.z + 0.1f);
        }

        currentColor += 1;
        if (currentColor > THIS.colors.Count - 1)
            currentColor = 0;
    }

    private static void ResetColor(SpriteRenderer sr, Color color) {
        sr.transform.localPosition = new Vector3(sr.transform.localPosition.x, sr.transform.localPosition.y, 0);
        sr.transform.localScale = Vector3.zero;
        sr.color = new Color(color.r, color.g, color.b, 1);
    }
	#endregion
}
